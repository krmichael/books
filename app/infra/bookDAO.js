function bookDAO(connection) {
  this._connection = connection;
}

bookDAO.prototype.lista = function(callback) {
  this._connection.query('select * from books', callback);
}

bookDAO.prototype.salva = function(book, callback) {
  this._connection.query('insert into books set ? ', book, callback);
}

bookDAO.prototype.apaga = function(idbook, callback) {
  this._connection.query('delete from books where id = '+idbook, callback);
}

bookDAO.prototype.filtraPorId = function(idbook, callback) {
  this._connection.query('select * from books where id = '+idbook, callback);
}

bookDAO.prototype.atualiza = function(idbook, dados, callback) {
  this._connection.query('update books set ? where id = '+idbook, dados, callback);
}

module.exports = function() {
  return bookDAO;
}