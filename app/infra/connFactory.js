const mysql = require('mysql2');

function createDBConnection() {

  return mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'silva!$!$',
    database: 'book'
  });
}

//Wrapper
module.exports = function() {
  return createDBConnection;
}