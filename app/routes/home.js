module.exports = function(app) {

  app.get('/', function(req, res) {
    
    var connection = app.infra.connFactory();
    var book = new app.infra.bookDAO(connection);

    book.lista(function(err, results) {

      if(err) {
        console.log(err);
      }

      res.format({
        html: function() {
          return res.render('home', { title: 'Home', lista: results });
        },
        json: function() {
          return res.json(results);
        }
      });
    });

    connection.end();
  });

  app.delete('/delete/:id', function(req, res) {

    var connection = app.infra.connFactory();
    var book = new app.infra.bookDAO(connection);
    
    book.apaga(req.params.id, function(err, results) {

      if(err) {
        console.log(err);
      }

      res.redirect('/');
    });

    connection.end();
  });

  app.get('/edit/(:id)', function(req, res) {

    var connection = app.infra.connFactory();
    var book = new app.infra.bookDAO(connection);

    book.filtraPorId(req.params.id, function(err, results) {

      if(err) {
        console.log(err);
      }

      res.render('edit', { title: 'Editar', lista: results });
    });

    connection.end();
  });

  app.put('/edit/(:id)', function(req, res) {

    var connection = app.infra.connFactory();
    var book = new app.infra.bookDAO(connection);

    book.atualiza(req.params.id, req.body, function(err, results) {

      if(err) {
        console.log(err);
      }

      res.redirect('/');
    });

    connection.end();
  });
}