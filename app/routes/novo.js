const { check, validationResult } = require('express-validator/check');

module.exports = function (app) {

  app.get('/novo', function (req, res) {
    res.render('novo', { title: 'Cadastrar novo ebook', errosValidacao: {}, book: {} });
  });


  app.post('/novo', [
    check('nome', 'Campo obrigátorio').not().isEmpty(),
    check('descricao', 'Campo obrigátorio').not().isEmpty(),
  ], (req, res) => {

    const book_data = req.body;
    const erros = validationResult(req);
    
    if(!erros.isEmpty()) {
      res.format({
        html: () => res.status(400).render('novo', {
          title: 'Erro ao cadastrar book', errosValidacao: erros.array(), book: book_data
        }),
        json: () => res.status(400).json(erros.array())
      });

      return;
    }

    var connection = app.infra.connFactory();
    var book = new app.infra.bookDAO(connection);

    book.salva(book_data, (err, results) => {
      err ? console.log(err) : res.redirect('/');
    });

    connection.end();
  });
};