const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const consign = require('consign');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const methodOverride = require('method-override');

module.exports = function() {

  var app = express();

  app.use(express.static('./app/public'));
  app.set('view engine', 'ejs');
  app.set('views', './app/views');
  app.use(expressLayouts);

  app.use(bodyParser.urlencoded({extended: true}));
  app.use(expressValidator());
  app.use(methodOverride('_method'));

  consign({cwd: 'app'})
    .include('routes')
    .then('infra')
    .into(app);

  return app;
}